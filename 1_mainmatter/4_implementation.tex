\chapter{Ascon im NoC}
\label{sec:methods}

Es gibt mehrere Ansätze, wie Ascon in unserem Protokoll eingesetzt werden kann.
Eine abgeschlossene authentisierte Verschlüsselung jedes einzelnen Flits
würde zwar im Falle einer Flitverlust bzw. einer Flitmodifikation
eine Retransmission eines einigen Flits bieten können,
der Overhead und die Verarbeitungsdauer würden das leichtgewichtige Ascon allerdings
in ein schwergewichtiges umwandeln.
In diesem Kapitel werden wir diese zwei Ansätze genauer betrachten,
miteinander und mit dem Kommunikationsprotokoll von Haase u. a. vergleichen.
Für das einfachere Unterscheiden zwischen Protokollen
geben wir unseren modifizierten Protokollen folgende
in sich die Begriffe Ascon und NoC verschmelzende Arbeitsnamen:
AsNoC/1 und AsNoC/2.



%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SECTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{AsNoC/1}
Mit AsNoC/1 beschreiben wir einen Ansatz,
in welchem eine gesamte Nachricht zwischen zwei Verarbeitungselementen
verschlüsselt und samt Routingdaten authentisiert wird,
wobei der Schlüsseltext zwischen mehreren Flits verteilt wird.
Die Funktionen der drei Flittypen werden
im Vergleich zu dem Protokoll von Haase u. A. leicht geändert werden.
In AsNoC/1 werden die Flittypen nicht mithilfe von Hardware-Signalen,
sondern mittels 2 Bits am Anfang jedes Flits unterschieden.

\begin{table}[h]
	\centering
	\caption{
		Belegung der ersten zwei Bits eines Flits in AsNoC/1.
		}
	\label{tab:flit_type}
	\renewcommand{\arraystretch}{1.3}
	\begin{tabular}{llll}
		\toprule
        Erster Bit       & Zweiter Bit        & Datenflits    & Serviceflits \\ \midrule
		0                & 0                  & Headerflit    & -            \\
		0                & 1                  & Bodyflit      & -            \\
		1                & 0                  & Tailflit      & -            \\
		1                & 1                  & -             &  ACK-Flit    \\ \bottomrule
	\end{tabular}
\end{table}


Es gibt 3 Typen von Datenflits
--- Headerflit ($00_2$), Bodyflit ($01_2$) und Tailflit ($10_2$) ---,
sowie einen Serviceflit ($11_2$),
der im Moment lediglich die Funktion eines Acknowledgement-Flits ausübt
(s. Tabelle \ref{tab:flit_type}).


Der Headerflit (s. Abbildung \ref{fig:my_headerflit})
enthält nicht nur Routinginformationen,
sondern besteht zur Hälfte aus Nutzdaten.
An der Stelle müssen wir betonen,
im Vergleich zu von Haase u. a. beschriebenem Protokoll
enthält der Headerflit in AsNoC/1 keinen \ac{MAC},
daher kann keine sofortige Authentifikation nach dem Erhalt des Headerflits erfolgen.
Es kann kein \ac{ACK} an den Sender versandt werden.
Erst mit dem Erhalt des Tailflits kann die Integrität geprüft werden.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{fig/my_headerflit.png}
	\caption{Struktur des Headerflits in AsNoC/1.}
	\label{fig:my_headerflit}
\end{figure}

Für die weitere Optimierung des Headerflits im Protokoll von Haase u.a. kann diskutiert werden,
ob der Block mit Modus, Länge und Paket-ID tatsächlich 40 Bit benötigt.
Die Paket-ID im beschriebenen Protokoll beträgt 32 Bit,
welche zwischen $2^{32} = 4'294'967'296$ Paketen unterscheiden lässt.
Wenn für die Identifizierung der Pakete
auch die Routing-Informationen wie Start- und Zieladresse in Betracht gezogen würden,
könnten theoretisch
--- je nach der Größe des \ac{NoC}-Netzes ---
bis 24 Bit in der \texttt{Packet ID} eingespart werden.
In AsNoC/1 wurde die Länge der Paket-ID von $32 Bit$ auf $26 Bit$ reduziert ---
damit lassen sich $67'108'864$ Pakete unterscheiden.
Wenn wir die Adressierungskoordinaten auch als einen Teil der ID betrachten,
bekommen wir eine Paket-ID der Größe $32 + 16 = 48 Bit$.
Damit kann ein \ac{VE} jeweils $67'108'864$ Pakete an jedes andere \ac{VE} senden.

In AsNoC/1 gibt es kein Modus-Feld,
da die Flit-Art am Anfang jedes Flits definiert wird,
womit 2 Bit eingespart werden.

Das Länge-Feld heißt in AsNoC/1 Nutzdatengröße 
und gibt die Größe der Nutzdaten in Byte wieder.
Es ist 12 Bit lang,
damit eine Nutzdatenlänge von $0$ bis $2^{12} = 4096$ Byte definiert werden kann.
Dabei ist eine maximale Länge $L_{max}$ der Nutzdaten 3784 Byte:
\begin{align*}
	L_{max} &= 256 Bodyflits * 118 \frac{Bit}{Bodyflit} + 1 Headerflit * 64 \frac{Bit}{Headerflit} \\
	&= 3784 Byte < 2^{12} Byte = 4096 Byte.
\end{align*}

An der Stelle kann diskutiert werden,
ob die Länge der Nutzdaten tatsächlich angegeben werden muss.
Für eine korrekte Padding-Behandlung könnte die Angabe der Padding-Größe ausreichen.
Sie würde nur $ \lceil ld(110) \rceil = 7$ Bit benötigen,
da das längste Padding nur im Bodyflit entstehen kann
und ist gleich $ 118 Bit (Nutzdatenbereich) - 8 Bit (1 Byte Nutzdaten) = 110 Bit (Padding) $.
Die Angabe der Anzahl der Flits in AsNoC/1 erscheint uns redundant,
da der Flit-Fluss mit einem Tailflit beendet wird
eher die Authentifikation beginnt.
Für den Fall, dass ein Tailflit verloren geht bzw. seine Kennzeichnung manipuliert wird,
kann ein Timeout-Mechanismus eingebaut werden,
welcher bei dem Erhalt eines Headerflits aktiviert wird
und bei der Zeitüberschreitung für das Beenden der Austauschsitzung sorgt.

Der gesamte Headerflit außer des Kennzeichens $00$ wird authentisiert.
Die Nutzdanten werden verschlüsselt.
Die Paket-ID und die Nutzdatengröße können ggf. auch verschlüsselt werden.
Die 4 Koordinatenblöcke bleiben unverschlüsselt,
werden aber authentisiert.

Die Bodyflits (s. Abbildung \ref{fig:my_bodyflit})
enthalten eine 10 Bit lange, nicht verschlüsselte und nicht authentisierte Flitkennzeichnung,
welche aus dem Flittyp $01$ (2 Bit) und der Flit-ID (8 Bit) besteht,
und einen 118 Bit langen Block mit verschlüsselten und authentisierten Nutzdaten.
Die Flit-ID fängt mit der Zahl $00000000_2$,
welche mit jedem folgenden Bodyflit um 1 inkrementiert wird.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{fig/my_bodyflit.png}
	\caption{Struktur des Bodyflits in AsNoC/1.}
	\label{fig:my_bodyflit}
\end{figure}

Der Tailflit (s. Abbildung \ref{fig:my_tailflit}) enthält keine Nutzdaten,
sondern lediglich einen kurzen, nicht verschlüsselten und nicht authentisierten Flittyp (2 Bit)
und den Tag $T$ (126 Bit).

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{fig/my_tailflit.png}
	\caption{Struktur des Tailflits in AsNoC/1.}
	\label{fig:my_tailflit}
\end{figure}

Mit dem Erhalt eines Tailflits beginnt die Authentifikation des Pakets.
Erst nach einer positiven Authentifikation des gesamten Pakets
wird ein \ac{ACK}-Flit mit der Bestätigung des korrekten Empfanges eines Pakets an den Sender versandt.
Dabei kann der ACK-Flit in AsNoC/1
eine ähnliche Struktur wie im Protokoll von Haase u.a. behalten
(siehe Abbildung \ref{fig:ack}).

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{fig/my_ack_flit.png}
	\caption{Struktur des Acknowledgement-Flits in AsNoC/1.}
	\label{fig:ack}
\end{figure}

Damit ist ein Senden-Empfangen-Zyklus zu Ende.
Das Aussehen eines Pakets in AsNoC/1 kann mit folgender Formel zusammengefasst werden:
\[
	H \ \ B^l \ \ T 
\]
mit Headerflit $H$, Bodyflit $B$, Tailflit $T$ und Anzahl der Bodyflits $l$,
wobei $0 \leq l \leq 256$.

In AsNoC/1 bekommt der Service-Flit ein 2 Bit langes Flitkennzeichen $11_2$,
und die Größe der Paket-ID wird auf 26 Bit reduziert (s. Beschreibung des Headerflits).
Der 2 Bit lange Platzhalter mit der Bezeichnung Modusfeld ist mit $00$ initialisiert,
was den Typ des Service-Flits als \ac{ACK} definiert.
An der Stelle kann diskutiert werden,
ob andere Service-Flits gebraucht werden.
\ac{ARQ}s würden in diesem Protokoll überflüssig,
da die Flits nicht einzeln authentisiert werden.
Zwar werden die Flits mit einer aufsteigenden Flit-ID versehen
und es nicht schwer ist,
einen verlorenen Flit nach der Lücke in der Reihenfolge der Flit-IDs zu identifizieren,
darf nicht vergessen werden,
dass die Flit-IDs weder verschlüsselt noch authentisiert werden,
was dem Angreifer ermöglicht,
die zuerst unbemerkt zu manipulieren.

Im Falle einer idealen Übertragung eines aus 258 Flits
(1 Headerflit, 256 Bodyflits, 1 Tailflit) bestehenden Pakets
wird die kleinstmögliche Overhead-Größe $R_{AsNoC\_1}$ von 8 \% erwartet:
\begin{align*}
	R_{AsNoC/1}	&= 1 - \frac{ 1 Headerflit * 64 \frac{Bit}{Headerflit}
	+ 256 Bit * 118 \frac{Bit}{Bodyflit}
	+ 1 Tailflit * 0 \frac{Bit}{Tailflit}}
	{ 258 Flits * 128 \frac{Bit}{Flit}} \\
	&= 1 - 0,92 \\
	&= 0,08
\end{align*}

Die Tabelle \ref{tab:asnoc1overhead} zeigt,
dass bei einer idealen Übertragung der größeren Pakete
der Anteil an Nutzdaten in AsNoc/1 sich
im Vergleich zum Protokoll von Haase u.a. verdoppelt.

\begin{table}[h]
	\centering
	\caption{
		Größe des Overheads in AsNoC/1
		mit Paketen verschiedener Größe.
		Vergleich zum originalen Protokoll von \textit{Haase u.a.}.
		}
	\label{tab:asnoc1overhead}
	\renewcommand{\arraystretch}{1.3}
	\begin{tabular}{p{1.3cm}p{1.8cm}p{2.3cm}p{2.3cm}p{2.3cm}p{2.2cm}}
		\toprule
        Anzahl Flits      & Padding     & Nutzdaten, Bit & Gesamtdaten, Bit & Anteil Nutzdaten, \% & Verbesserung, \% \\
        \midrule
		64                & nein        & 7380           & 8192             & 90,0 &  47,0 \\
		32                & nein        & 3604           & 4096             & 88,0 & 45,6 \\
		16                & nein        & 1716           & 2048             & 83,8 & 42,9 \\
		8                 & nein        & 772            & 1024             & 75,4 & 37,4 \\
		4                 & nein        & 300            & 512              & 58,5 &  25,7 \\
		3                 & nein        & 182            & 384              & 47,3 &   18,1 \\ 
		2                 & nein        & 64             & 256              & 25,0 &  3,1 \\
		2                 & ja          & 8              & 256              & 3,1  &  0,0 \\
        \bottomrule
	\end{tabular}
\end{table}



\subsection{Angriffe unter AsNoC/1}
Während AsNoC/1 bei einer idealen Übertragung
durch ein geringeres Overhead effizient zu sein scheint,
kann ein \ac{NoC} mit aktivierten Trojaner die Effizienz drastisch senken.
Der Grund dafür liegt in den fehlenden \ac{ARQ}s,
die wenig sinnvoll in Paketen,
deren Flits nicht einzeln authentisiert werden,
sind.

Die Tabelle \ref{tab:asnoc1_angriffe}
zeigt mögliche Angriffsszenarien eines potenziellen Angreifers.
Im idealen Fall
--- wenn der Angreifer nichts unternimmt ---
erreicht das Paket den Empfänger
und der Sender wird darüber mithilfe eines ACK-Flits informiert.
Der Sender sendet sein Paket an den Empfänger
und aktiviert dabei den Timeout-Zähler.
Bei dem ersten beim Empfänger angekommen Flit
--- Headerflit ---
wird die Übertragungssitzung an der Seite des Empfängers gestartet:
der Timeout-Zähler wird aktiviert,
der Headerflit und all die als nächstes ankommenden Flits werden gespeichert
und je nach Ankunft entschlüsselt.
Nach dem Empfang des letzten Flits
--- Tailflit ---
wird das gesamte Paket authentifiziert,
ein Acknowledgement-Flit wird an den Sender versandt
und die Übertragunssitzung wird damit geschlossen.

\begin{table}[h]
	\centering
	\caption{
		ToDo Szenarien der Angriffe in AsNoC/1 unter der Annahme,
		dass der Sender ein Paket versendet und
		keine Übertragungsfehler auftreten.
		}
	\label{tab:asnoc1_angriffe}
	\renewcommand{\arraystretch}{1.7}
	\begin{tabular}{ | p{4.5cm} | p{1.5cm} | p{3cm} | p{4.8cm} | }

		\hline
		\textbf{Angreifer} &
		\textbf{Sitzung aktiviert} &
		\textbf{Sender} &
		\textbf{Empfänger} \\
        \hline

		unternimmt nichts
		&
		ja
		&
		erhält ACK
		&
		erhält Paket
		\\ \hline

		löscht Headerflit
		&
		\multirow[t]{3}{=}{nein}
		&
		\multirow[t]{8}{=}{(1) Timeout: ACK fehlt, \newline \newline
		(2) ggf. Congestion-Window anpassen, \newline \newline
		(3) Paket erneut senden}
		&
		Ø
		\\ \cline {1-1} \cline{4-4}

		modifiziert die Kennzeichnung des Headerflits: $00$→$01$ bzw. $00$→$10$
		&
		&
		&
		Flit wegwerfen
		\\ \cline {1-1} \cline{4-4}
		
		modifiziert die Kennzeichnung des Headerflits: $00$→$11$ &
		&
		&
		Flit w. als ACK behandelt und Authentifikation schlägt fehl
		\\  \cline {1-2} \cline{4-4}

		modifiziert den Rest im Headerflit
		&
		\multirow[t]{4}{=}{ja}
		&
		&
		\multirow[t]{2}{=}{spätere Authentifikation schlägt fehl, oder Timeout: Tailflit fehlt}
		\\ \cline {1-1}

		modifiziert/löscht Bodyflit
		&
		&
		&
		\\ \cline {1-1} \cline{4-4}

		löscht Tailflit oder ändert die Flittypkennzeichnung
		&
		&
		&
		Timeout: Tailflit fehlt
		\\ \cline {1-1} \cline{4-4}
		
		modifiziert Tailflit
		&
		&
		&
		Authentifikation schlägt fehl
		\\ \cline {1-2} \cline{4-4}

		löscht ACK-Flit
		&
		\multirow[t]{4}{=}{nein}
		&
		&
		\multirow[t]{2}{=}
		{beim späteren Erhalt eines Headerflits mit der bekannten Paket-ID
		wird sofort ein ACK weiderholt gesendet,
		keine Sitzung gestartet}
		\\ \cline {1-1} \cline {3-3}

		modifiziert ACK-Flit
		&
		&
		(1) Authentifikation schlägt fehl;
		\newline \newline
		(2) Paket erneut senden
		&
		\\ \hline
		
	\end{tabular}
\end{table}

Damit eine Übertragungssitzung überhaupt gestartet werden kann,
muss ein Flit mit der Kennzeichnung $00$
--- was dem Kennzeichen eines Headerflits entspricht ---
ankommen.
Falls die Integritätsprüfung nach dem Empfang des Tailflits fehlschlägt,
ohne zu wissen,
woran es lag
--- ob an modifizierten Kennzeichen, Headerinformationen, Payload,
verlorenen Flits ---,
werden all die empfangenen Flits gelöscht,
und ohne ein ACK-Flit an den Sender zu senden,
wird die Sitzung beendet.
Die Sitzung wird auch dann beendet,
wenn der Empfänger-Timeout ausgeht bevor der Tailflit ankommt.
In den letzten zwei Fällen
--- da kein ACK-Flit den Sender erreicht ---
wird der Sender versuchen,
das Paket nochmal zu senden.

An der Stelle kann ein Congestion-Control (ToDo Referenz) 
--- ähnlich wie in den Rechnernetzen ---
verwendet werden.
Bei einem fehlenden ACK kann das Congestion-Window (ToDo Was ist das?) angepasst werden:
die letzte Nachricht wird in kleinere Pakete
--- z.B. je 4 Flits pro Paket ---
aufgeteilt und versendet.
Bei einer Anzahl $n_{ACK}$ an hintereinander angekommenen ACKs
und damit erfolgreich zugestellten Paketen
kann das Congestion-Window peu à peu erhöht werden.
Damit kommt es zur Reduzierung der Anzahl an Flits,
die bei einem fehlenden ACK
erneut gesendet werden müssen.
Für die Implementierung dieses Verfahrens wird extra Speicher benötigt,
um eine Tabelle mit potenziellen Zielknoten (x, y)
und dazu gehörigen Congestion-Window-Parameter zu speichern.
An der Stelle wird diese Idee nicht weiter vertieft
(oder doch in der Implementierung?).

Der letzte Flit,
der vom Angreifer gelöscht bzw. modifiziert werden kann,
ist der ACK-Flit selbst.
In den beiden Fällen wird der Sender versuchen,
das Paket erneut an den Empfänger zu senden.
Wenn der Empfänger den Headerflit mit der ID des Paketes,
welches bereits erfolgreich empfangen wurde,
erhält,
wird keine Sitzung gestartet ---
der Headerflit und all die danach kommenden Body- und Tailflits
werden gelöscht.

In diesem Abschnitt wurde gezeigt,
dass bei jeder Modifizierung bzw. Verwerfung der Flits eines Pakets
muss das ganze Paket erneut gesendet werden.
Je öfter solche Angriffe stattfinden
und je größer die Pakete sind,
desto uneffizienter wird das \ac{NoC} mit AsNoC/1.
Es wurde gezeigt,
dass ein Congestion-Control dieses Problem mildern könnte.



\subsection{Verschlüsselung und Authentisierung in AsNoC/1}
Für die Verschlüsselung und Authentisierung der Pakete in AsNoC/1
wird \textsc{Ascon-128} verwendet.
Das gesamte Verschlüsselungs- und Authentisierungsprozedere
(s. Abbildung \ref{fig:ascon_verschl} als Wiederholung)
--- die vier Stufen ---
wird einmal pro Paket durchgeführt.

In der Initialisierungsphase
wird der Initialisierungsvektor $IV$,
konkateniert mit dem Schlüssel $K$
und der Nonce $N$
--- alles jeweils 128 Bit lang ---,
12 mal permutiert.
Der Initialisierungsvektor $IV = \mathtt{8040 0C06 0000 0000}_{hex}$
wird aus folgenden Komponenten zusammengestellt:
\begin{itemize}
    \item Schlüsselgröße $128_{10} = \mathtt{80}_{hex} Bit$,
    \item Blockgröße $64_{10} = \mathtt{40}_{hex} Bit$,
    \item Anzahl der Runden der Permutation $p^a$ $12_{10} = \mathtt{0C}_{hex}$,
	\item Anzahl der Runden der Permutation $p^b$ $6_{10} = \mathtt{06}_{hex}$,
	\item Padding $\mathtt{0000 0000}$.
\end{itemize}

Um die Generierung und den Austausch der Schlüssel
für eine asymmetrische Verschlüsselung bzw. Authentisierung
kümmert sich das \ac{KGC}.
Die Nonce $N$ kann aus der ersten Hälfte des Headerflits
--- falls Paket-ID und Nutzdatengröße nicht verschlüsselt werden ---
bestehen.
Die restlichen 64 Bit können entweder aus Nullen
oder nochmal aus der ersten Hälfte des Headerflits
bestehen.
Falls sowohl die Paket-ID als auch die Nutzdatengröße
verschlüsselt werden,
muss das \ac{KGC} auch die Aufgabe der Verteilung der Noncen übernehmen.

In der zweiten Phase des Asnoc-128-Verfahrens
werden solche Headerdaten wie
Knotenadressen,
Paket-ID und
Nutzdatengröße
authentisiert.
Falls die Nonce bereits aus diesen Daten bestand,
könnte dieser Schritt ausgelassen werden (ToDo Warum?).

In der dritten Phase wird der Payload verschlüsselt.
Die verschlüsselten 64 Bit großen Blöcke
müssen ggf. geteilt werden,
bevor sie zwischen den Header- und Bodyflits verteilt werden können.
Der letzte Payload-Block darf eine Größe zwischen $0$ und $64$ Bit haben
und braucht kein Padding.

In der letzten Phase wird ein 126 Bit langer Tag extrahiert
und dem Bodyflit hinzugefügt.

Die Authentifikation und die Entschlüsselung verlaufen ähnlich
in derselben Reihenfolge (s. Abbildung \ref{fig:ascon_entschl}).

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{fig/ascon_entschl.png}
	\caption{
        Die vier Stufen der Entschlüsselung in \textsc{Ascon-128}.
    }
	\label{fig:ascon_entschl}
\end{figure}


\subsection{Interne Notizen}
In order to fulfill the security claims stated in Table 8, imple-
mentations must take care that the nonce (public message number) is never repeated for
two encryptions under the same key, and that decrypted plaintexts are only released after
successful verification of the final tag.

Performance? Siehe S. 33 Ascon 1.2, siehe WebSeite eBACS




%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% SECTION %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
\section{AsNoC/2}
Mit AsNoC/1 haben wir ein overheadarmes Kommunikationsprotokoll beschrieben,
welches in einem \ac{NoC},
wo es wenige Angriffe auf die übertragenen Flits gibt,
effizient funktionieren kann.
Wegen fehlender \ac{ARQ}s
kann allerdings keine Anforderung an Übertragung eines einzelnen Flits
gesandt werden.
Mit AsNoC/2 versuchen wir
dieses Problem zu lösen.
Dafür haben wir die Architektur der Pakete überarbeitet
und die Funktionen der Flits (s. Tabelle \ref{tab:asnoc2_flits}) angepasst.

\begin{table}[h]
	\centering
	\caption{
		Flit-Typen im Protokoll AsNoC/2.
		}
	\label{tab:asnoc2_flits}
	\renewcommand{\arraystretch}{1.3}
	\begin{tabular}{ll}
		\toprule
		Kennzeichen & Bedeutung \\
		\midrule
		0000        & 1. Bodyflit \\
		0001        & 2. Bodyflit \\
		0010        & 1. Bodyflit mit Padding \\
		0011        & 2. Bodyflit mit Padding \\
		0100        & 1. Bodyflit bei Retransmission \\
		0101        & 2. Bodyflit bei Retransmission \\
		0110        & 1. Bodyflit mit Padding bei Retransmission \\
		0111        & 2. Bodyflit mit Padding bei Retransmission \\
		1000        & Headerflit \\
		1001        & Headerflit bei Retransmission \\
		1010        & Tagflit \\
		1011        & Tailflit (letzter Tagflit eines Paketes)\\
		1100        & ACK-Flit \\
		1101        & ARQ-Flit \\
		1110        & ACK-Flit bei Retransmission \\
		1111        & ARQ-Flit bei Retransmission \\
		\bottomrule
	\end{tabular}
\end{table}

Als Ausgangspunkt nehmen wir das Protokoll von Haase u.a.
und gestalten unseren Headerflit (s. Abbildung \ref{fig:asnoc2-headerflit}) in einer ähnlichen Art und Weise:
die gesamten Routinginformationen samt Paketinformationen
werden nicht verschlüsselt aber sofort authentisiert.
Ein Headerflit in AsNoC/2 besitzt
ein Kennzeichen $1000$,
ein 24 Bit langes Adressierungsfeld,
eine 28 lange Paket-ID ($268'435'456$ Möglichkeiten ToDo ist Leerzeichen klein genug?),
ein 8 Bit langes Feld für die Anzahl der folgenden Flit-Gruppen und
einen 64 Bit langen MAC.
Mit dieser Initialsierung lassen sich $2^8 + 1 = 257$ Flitgruppen bilden.
Wenn jede Gruppe 2 Bodyflits mit jeweils $116 Bit$ Nutzdaten enthält,
dann können $116 Bit * 2 (Flits \, pro \, Gruppe) * 257 (Flitgruppen) = 59624 Bit = 7453 Byte$
innerhalb eines Pakets übertragen werden.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{fig/asnoc2-headerflit.png}
	\caption{
        Die Struktur eines Headerflits in AsNoC/2.
    }
	\label{fig:asnoc2-headerflit}
\end{figure}

Für die Verschlüsselung und Authentisierung wird wiederum \textsc{Ascon-128},
analog zu (ToDo Referenz zum Kapitel), verwendet.
Die Nonce kann gleich wie in AsNoC/1 gebildet werden:
aus den ersten 64 Bit des Headerflits.

Nach dem Empfang und einer erfolgreichen Authentifizierung des Headerflits
wird ein Acknowledgement-Flit an den Sender gesandt.
Der ACK-Flit in AsNoC/2 hat eine ähnliche Struktur wie der gleiche Flit im Protokoll von Haase u.a.
(siehe Abbildung \ref{fig:asnoc2-ackflit}).
Dabei können analog zur Header-Nonce
die ersten 64 Bits des ACK-Flits genommen werden.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{fig/asnoc2-ackflit.png}
	\caption{
        Die Struktur eines ARQ-Flits in AsNoC/2.
    }
	\label{fig:asnoc2-ackflit}
\end{figure}

Die Nutzdaten werden in AsNoC/2 in Zweier- bzw. Dreier-Gruppen gesendet:
mit jeweils einem bzw. zwei Bodyflits und einem Tagflit.
Die Bodyflits und der Tagflit werden zusammen als Gruppe
mithilfe von \textsc{Ascon-128} verschlüsselt und authentifiziert.
Als Nounce kann der MAC vom Headerflit,
konkateniert mit der Flitgruppen-ID,
verwendet werden.

Der erste ($0000$) und der zweite ($0001$) Bodyflits haben dieselbe Struktur
(s. Abbildungen \ref{fig:asnoc2-bodyflit1} und \ref{fig:asnoc2-bodyflit2}).
Sie bestehen aus einem 4 Bit langen Kennzeichen,
einer 8 Bit langen Flitgruppen-ID und
einem 116 Bit langem Nutzdatenblock.
Mithilfe der Tagflits (Kennzeichen $1010$),
die jede Dreier-Gruppe abschließen,
werden die Daten dieser Flitgruppen authentifiziert.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{fig/asnoc2-bodyflit1.png}
	\caption{
        Die Struktur eines ersten in jeder Flitgruppe Bodyflits in AsNoC/2.
    }
	\label{fig:asnoc2-bodyflit1}
\end{figure}

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{fig/asnoc2-bodyflit2.png}
	\caption{
        Die Struktur eines zweiten in jeder Flitgruppe Bodyflits in AsNoC/2.
    }
	\label{fig:asnoc2-bodyflit2}
\end{figure}

Falls die Authentifikation misslingt bzw.
einige Flits nicht ankommen,
sendet der Empfänger einen \ac{ARQ} (ToDo Bildchen) mit der ID der Flitgruppe,
welche nachgesendet werden muss.
Bei der Retransmission bekommen die Body- bzw. Tagflits andere Kennzeichen
(s. Tabelle \ref{tab:asnoc2_flits}),
damit (ToDo warum?).

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{fig/asnoc2-arqflit.png}
	\caption{
        Die Struktur eines ARQ-Flits in AsNoC/2.
    }
	\label{fig:asnoc2-arqflit}
\end{figure}

Der Tagflit jeder letzten Flitgruppe eines Pakets wird als Tailflit bezeichnet:
er hat dieselbe Struktur wie der Tagflit (s. Abbildung \ref{fig:asnoc2-tailflit}),
allerdings ein anderes Flitkennzeichen --- $1011$.
Nur die Tailgruppe eines Pakets kann aus lediglich einem Bodyflit bestehen
bzw. einen Bodyflit mit Padding enthalten.
Falls der letzte Bodyflit ein Padding enthält,
wird er mit der Nummer $0010$ bzw. $0011$ gekennzeichnet.
Der Unterschied liegt darin,
ob es sich um den ersten und den letzten Bodyflit einer Gruppe
oder um den zweiten und den letzten handelt.
Ein Padding-Bodyflit enthält nach der Flit-ID einen Padding-Block flexibler Größe,
der mit Nullen beginnt und mit einer Eins endet: $(0^k \ 1)$ mit $0 \leq k \leq 115$.
Die restlichen Bits sind für die Nutzdaten vorgesehen.
Der Padding-Block wird zusammen mit dem Payload verschlüsselt
und erst nach der Entschlüsselung beim Empfänger entfernt.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{fig/asnoc2-tailflit.png}
	\caption{
        Die Struktur eines Tagflits ($x=0$) bzw. eines Tailflits ($x=1$).
    }
	\label{fig:asnoc2-tailflit}
\end{figure}

Damit ist ein Senden-Empfangen-Zyklus zu Ende.
Das Aussehen eines Pakets in AsNoC/2 kann mit folgender Formel zusammengefasst werden:

\[
	H \ \ (B_1 B_2 T_g)^l \ \ \big( (B_1 B_2 T) \ | \ (B_1 B_{2, pad} T) \  | \ (B_1 T) \ | \ (B_{1, pad} T) \big)
\]

mit Headerflit $H$, Bodyflit $B$, Tagflit $T_g$, Tailflit $T$ und Anzahl der Dreier-Gruppen $l$,
wobei $0 \leq l \leq 256$.



\subsection{Angriffe unter AsNoC/2}
Während die nicht angekommenen Pakete und \ac{ACK}s in AsNoC/1
ledigilich nach dem Ablauf eines Timeouts nachgesendet werden können,
verfügt AsNoC/2 über eine Möglichkeit,
aktiv ein Nachsenden zu veranlassen.
Dabei
--- im Gegenteil zu AsNoC/1 ---
können in AsNoC/2 konkrete Flits bzw. Flitgruppen
nachgefordert und geliefert werden.
Dafür werden \ac{ACK}s und \ac{ARQ}s
--- genauso wie im Protokoll von Haase u.a. ---
mit einer einzigen Änderung verwendet.
Während im Protokoll von Haase u.a.
die Flits eizeln verschlüsselt, authentisiert
und demenstsprechend bei der Verlust nachgefordert werden,
haben wir in AsNoC/2 mit Flitgruppen,
bestehend aus 2 bis 3 Bit,
zu tun.

In der Tabelle \ref{tab:asnoc2-angriffe}
wurden mögliche Angriffszenarien im \ac{NoC} mit AsNoC/2 zusammengefasst.
Im Vergleich zu AsNoC/1 kommt es in AsNoC/2 dank den \ac{ARQ}s seltener vor,
dass ein ganzes Paket wiederholt gesandt wird.

\begin{table}[h]
	\centering
	\caption{
		Szenarien der Angriffe in AsNoC/2 unter der Annahme,
		dass der Sender ein Paket versendet und
		keine Übertragungsfehler auftreten.
		}
	\label{tab:asnoc2-angriffe}
	\renewcommand{\arraystretch}{1.7}
	\begin{tabular}{ | p{4.5cm} | p{4.2cm} | p{5cm} | }

		\hline
		\textbf{Angreifer} &
		\textbf{Sender} &
		\textbf{Empfänger} \\
        \hline

		unternimmt nichts
		&
		erhält ACK
		&
		erhält Paket
		\\ \hline % \cline {1-1} \cline{4-4} \multirow[t]{3}{=}{nein}

		löscht Headerflit
		&
		\multirow[t]{4}{=}{Timeout: ACK fehlt; \newline\newline sendet Retransmission-Headerflit}
		&
		\multirow[t]{2}{=}{verwirft Body-, Tag- und Tailflits}
		\\ \cline {1-1}

		modifiziert das Kz. des Headerflits in \mathtt{0xxx} oder \mathtt{101x}
		&
		&
		\\ \cline {1-1} \cline{3-3}

		modifiziert das Kz. des Headerflits in \mathtt{11xx}
		&
		&
		\multirow[t]{2}{=}{Authentifikation schlägt fehl; das Paket w. verworfen}
		\\ \cline {1-1}

		modifiziert den Rest im Headerflit
		&
		&
		\\ \hline

		modifiziert/löscht ein Body-, Tag- oder Tailflit
		&
		authentifiziert ARQ; sendet die angefragte Flitgruppe mit Kz. „Retransmission”
		&
		sendet ARQ mit der ID der fehlenden Flitgruppe
		\\ \hline

		löscht ACK-Flit
		&
		Timeout: ACK fehlt; sendet Paket erneut mit Retransmission-Headerflit.
		&
		sendet Retransmission-ACK, verwirft Flits
		\\ \hline

		modifiziert ACK-Flit
		&
		Authentifikation schlägt fehl; sendet Retransmission-Headerflit
		&
		sendet Retransmission-ACK, verwirft Flits
		\\ \hline

		löscht ARQ-Flit
		&
		Ø
		&
		\multirow[t]{2}{=}{Timeout: Flitgruppe fehlt; sendet Retransmission-ARQ}
		\\ \cline {1-2}

		modifiziert ARQ-Flit
		&
		Authentifikation schlägt fehl; ARQ-Flit wird verworfen
		&
		\\ \hline		
	\end{tabular}
\end{table}







\section{Simulation mit OMNeT++}
ToDo