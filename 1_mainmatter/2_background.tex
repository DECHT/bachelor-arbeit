\chapter{Grundlagen}
\label{sec:background}

\section{Was ist NoC}

In der Ära des \ac{IoT},
in der die Anzahl der Smart-Geräte die Bevölkerungszahl übertrifft,
wobei das Volumen solcher Geräte abnimmt
während deren Leistung steigt,
gewinnen kompakte, aber dennoch leistungsstarke Recheneinheiten
erheblich an Bedeutung.

Das „Kernstück” solcher Geräte (wie z. B. Smartwatches)
wird nun auf einem einzigen Chip platziert.
Solche komplexen, aber kompakten Systeme --- genannt \ac{SoC} ---
findet man heute in vielen Bereichen:
von Smart-Home-Geräten
bis zu den komplexen Navigationssystemen in Flugzeugen \cite{SurveyOnNoC}.

Ein \ac{SoC} besteht aus mehreren miteinander kommunizierenden Verarbeitungselementen.
Bei einer großen Anzahl an Verarbeitungselementen
würde eine globale Busverbindung
zum Flaschenhals werden und
den Datendurchsatz massiv beeinträchtigen.
Für ein Routing über mehrere Wege ohne einen globalen Bus
wird \ac{NoC} eingesetzt.
Eine wichtige Rolle spielt dabei die Topologie,
die in einem \ac{NoC} verwendet wird ---
z. B. Gitter, Ring, Torus, Stern ---,
denn sie definiert
die Länge und die Anzahl möglicher Kommunikationswege.
In dieser Arbeit wird ein \ac{NoC}
mit einem 2D-Gitternetz betrachtet.


\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{fig/2x2NoC.png}
	\caption{
		Ein \ac{NoC} mit einem 2x2-Gitternetz:
		VE --- Verarbeitungselement,
		NA --- Netzwerkadapter,
		R --- Router.
		}
	\label{fig:2x2NoC}
\end{figure}


In einem 2x2-Gitternetz (s. Abbildung \ref{fig:2x2NoC})
kommunizieren 4 Verarbeitungselemente miteinander.
Diese Elemente können bspw.
Prozessor, Speicher, Grafikkarte oder Ein-/Ausgabe-Kontroller sein.
Die Kommunikation zwischen den Elementen erfolgt nicht direkt,
sondern mithilfe der Router.
Die Idee,
in \ac{SoC} ein Routingprotokoll mit für es charakteristischen Funktionen, wie
Flow-Kontrolle, Switching, Wegentscheidung und Puffern
zu verwenden,
stammt aus der traditionellen Netzwerk-Lösung,
wie beispielsweise Internet.

Im \ac{NoC} kommuniziert ein \ac{VE} $A$ mit einem \ac{VE} $B$
mithilfe von Nachrichten.
Eine Nachricht wird von jedem \ac{VE} in Pakete aufgeteilt
und diese Pakete werden an den entsprechenden \ac{NA} weitergegeben.
Der \ac{NA} bereitet die vom \ac{VE} erhaltenen Pakete für den Versand vor.
Jedes Paket wird in die kleinsten übertragbaren Einheiten
--- genannt \ac{Flit} ---
aufgeteilt und ggfs. verschlüsselt bzw. authentisiert.
Es gibt drei Typen von \ac{Flit}s:
\textit{Headerflit} enthält Routinginformationen,
\textit{Bodyflit} enthält Nutzdaten und
\textit{Tailflit} signalisiert das Ende eines Pakets.
Außer den Nutzdaten und Routinginformationen können \ac{Flit}s weitere Informationen enthalten,
wie z. B. Paketgröße,
Paket- und Flit-ID,
Modus,
Art des \ac{Flit}s usw.
Im Falle einer Authentisierung der Pakete
kann ein Teil der \ac{Flit}s zum Overhead werden,
indem er bspw. den \ac{MAC} in sich trägt.

Die Aufgabe eines Routers besteht darin,
\ac{Flit}s an andere Router weiterzuleiten bzw.
--- falls das ankommende \ac{Flit} für das vom Router betreute \ac{VE} bestimmt ist ---
das Paket an den entsprechenden \ac{NA} weiterzuleiten.
Der entsprechende \ac{NA} führt dann die erhaltenen \ac{Flit}s in ein Paket zusammen
führt ggfs. Entschlüsselungs--- und Verifikationsoperationen durch und
leitet letztendlich das Paket an das Ziel-\ac{VE}.



\section{Wie sicher ist NoC?}
Ein \ac{NoC}-basiertes \ac{SoC}
kann in allen drei Schutzzielen der CIA-Triade
(Confidentiality bzw. Vertraulichkeit,
Integrity bzw. Integrität und
Availability bzw. Verfügbarkeit)
--- je nach Angreifermodell ---
Verwundbarkeiten aufweisen.
Einerseits
kann ein passiver Angreifer
die übertragende Nachrichten an den Schnittstellen
mithilfe anderer Bauelemente abhören und weiterleiten,
andererseits kann ein modefiziertes Bauteil mit einem integrierten Trojaner
nicht nur passiv lauschen, sondern auch aktiv Nachrichten modifizieren
oder sogar selber solche Nachrichten erstellen und verbreiten.
Während ein infizierter \ac{NA} nur die ein- und ausgehende Flits bzw. Pakete,
die von ihm betreutes \ac{VE} betreffen,
behandeln,
können infizierte Router eine größere Gefahr darstellen,
denn sie leiten die Flits nicht nur vom zugehörigen \ac{NA},
sondern auch von anderen Routern weiter.


\subsection{Vertraulichkeit}
Bei unverschlüsselten Flits
kann ein böswilliger Router die durch ihn passierten Daten abfangen und speichern.
Die abgespeicherten Daten können dann via PAN (z. B. Bluetooth)
oder via Internet (eine infizierte Software ist erforderlich)
an den Interessenten weitergeleitet werden.
Eine Schutzstrategie könnte hier die Verschlüsselung sein.

Da für ein erfolgreiches Routing
die Start- und Ziel-Adressen
allen Routingteilnehmern sichtbar (unverschlüsselt)
zur Verfügung stehen,
können die böswilligen Router auch diese Informationen sammeln.
Dies kann mithilfe des Onion-Routings
--- einer Technik der anonymen Kommunikation ---
verhindert werden,
indem jedes Flit so oft verschlüsselt wird,
wie viele Knoten es auf dem Weg zum Ziel gibt \cite{onion} .
Bei einem Onion-Routing weiß jeder Zwischenknoten lediglich
wer der vorheriger (Zwischen-)knoten war
und an wen der Flit als nächstes weitergeleitet werden muss.
Ob es sich dabei eventuell um einen Start- bzw. Zielknoten handelt,
bleibt offen.
Allerdings ist die Implementierung
solch eines komplexen Onion-Verfahrens
in ressourcenbeschränkten \ac{SoC}s nicht praktikabel \cite{SurveyOnNoC}.


\subsection{Integrität}
Ein böswilliger Router kann mit Absicht die Daten vor dem Weiterleiten modifizieren.
Die modifizierten Flits können zu unerwarteten Fehlern beim Empfänger führen.
Eine Schutzstrategie könnte hier die Authentifizierung sein.
Um effizient sowohl Integrität als auch Vertraulichkeit zu schützen,
werden das \ac{AEAD} verwenden,
was im nächsten Kapitel näher vorgestellt wird.


\subsection{Verfügbarkeit}
Ein böswilliger Router kann weiterzuleitende Flits verwerfen bzw. fehlleiten,
was zur Fehlfunktion bzw. Überlastung des Systems führen kann.
Dasselbe Ziel erreicht der Angreifer 
mit böswilligen Manipulationen am Speicher bzw. Puffer.
Ein infizierter \ac{NoC}-Teilnehmer kann auch selbst Pakete erzeugen
und damit eine \ac{DoS}-Attacke ausüben.

Da böswillige Router oft nach besonderen Mustern
--- abhängig von der Struktur eines Flits ---
angreifen,
könnte das Vermischen der Bits innerhalb eines Flits helfen:
dabei ginge die Struktur des Flits kaputt
und der böswillige Router könnte seine Muster-Logik nicht mehr anwenden.
Dieses Verfahren gibt allerdings keine Sicherheitsgarantie
und schützt nicht vor \ac{DoS}-Attacken.

Um die Verfügbarkeit zu gewährleisten,
kann adaptives Routing,
welches sich bösartig verhaltende Komponente entdeckt und
eine Routingsänderung für die Vermeidung solcher Knoten initiiert,
in Einsatz gebracht werden.

In dieser Arbeit wird
der Schutz der Vertraulichkeit und Integrität
mithilfe einer authentisierten Verschlüsselung
unter die Lupe genommen,
welche indirekt den Schutz der Verfügbarkeit
mit sich bringt.


\section{NoC-Kommunikationsprotokoll von J.Haase u.a.}

J. Haase u. a. in \cite{Haase}
stellten ein sicheres Kommunikations-Protokoll für \ac{NoC} vor,
welches außer authentisierter Verschlüsselung
Recovery-Mechanismen mit sich bringt.
Das Protokoll sollte im Gegensatz zu den anderen in der Arbeit erwähnten Verfahren
vor Angriffen gegen alle drei Aspekte der CIA-Triade schützen:
vor Abhören und Vervielfältigung gegen die Vertraulichkeit,
vor Verfälschung und Man-in-the-Middle-Attacken gegen die Integrität, und
vor Fehlleitung und Verwerfung gegen die Verfügbarkeit.

In dieser Arbeit gelten die Router als nicht vertrauenswürdig,
sie können bspw. mit einem Trojaner infiziert sein.
Dagegen werden die restlichen Kommunikationsbeteiligten (\ac*{VE} sowie \ac{NA})
als absolut vertrauenswürdig betrachtet.
Es gibt ein neues Bauteil namens \ac{KGC} (s. Abbildung \ref{fig:2D_Gitter}),
welches sich um die Erzeugung und Verteilung der Schlüssel
für eine verschlüsselte und authentisierte Kommunikation kümmert.


\begin{figure}[ht]
	\centering
	\includegraphics[width=1\textwidth]{fig/2D_Gitter.png}
	\caption{
		Ein \ac{NoC} mit KGC (Key Generation Center).
		Im Vergleich zum davor illustrierten 2x2-Gitternetz,
		bestehend aus
		PE --- Verarbeitungselement,
		NA --- Netzwerkadapter und
		R --- Router,
		besitzt dieses ein KGC,
		welches sich um die Erzeugung und Verteilung der Schlüssel
		für eine verschlüsselte und authentisierte Kommunikation kümmert.
		Das KGC ist direkt mit den NAs verbunden.
		}
	\label{fig:2D_Gitter}
\end{figure}

Die Verschlüsselung samt Authentisierung
wurden in den \ac{NA}s implementiert.
Dafür wurde PRINCE 
--- eine leichtgewichtige symmetrische Blockchiffre ---
verwendet.
Die Schlüsselverteilung wurde mithilfe des Okamoto-Tanaka-Protokolls realisiert \cite{okamoto}.
Die Acknowledgements (ACKs),
die eine erfolgreich Ankuft eines Headerflits bestätigen,
und die Automatic-Repeat-Requests (ARQs),
die das erneute Senden eines Flits im Falle von Modifikationen oder Verlusten initiieren,
sorgen für die Verfügbarkeit und Integrität der gesandten Daten.

\subsection{Paketstruktur}

Ein vom \ac{VE} $A$ zum \ac{VE} $B$ zu übertragendes Paket
besteht aus mehreren \ac{Flit}s:
einem Headerflit,
einem Tailflit und
einer vordefinierten Anzahl (in unserem Fall von 0 bis 63) an Bodyflits.

Ein Headerflit (s. Abbildung \ref{fig:header_flit})
enthält die Routinginformationen (die Koordinaten der Kommunikationsknoten),
den Paket-Modus,
die Festlegung der maximalen Paketgröße und
die Paket-ID.
Die Adressen des Start- bzw. des Endknotens
werden mit jeweils 6 Bit großen Koordinaten $X$ und $Y$ festgelegt,
welche folglich ein NoC-Netz mit maximaler Größe von 64x64 aufspannen können.
Um zwischen den vier Modi --- „data”, „ARQ”, „ACK” und „ARQ response” --- zu unterscheiden,
werden 2 Bit benötigt.
Mit einer 32 Bit langen \textit{Packet ID}
lassen sich $2^{32} = 4'294'967'296$ verschiedene Pakete unterscheiden.
Der Block \textit{Length} definiert die maximale Größe eines Paketes,
im beschrieben Protokoll sind es 64 Flits.
Unklar ist,
ob es sich tatsächlich generell um eine maximal mögliche Größe handelt,
oder um die tatsächliche Größe eines konkreten Paketes.
Das letzte wäre nötig,
um ggfs. das entstehende Padding im letzten Flit mit Nutzdaten zu entfernen.
Die Größe dieses Blocks ist nicht angegeben,
sie lässt sich allerdings leicht anhand der gegebenen Daten ausrechnen:
$64 Bit (Payload) - 24 Bit (Adressierung) - 2 Bit (Modus) - 32 Bit (Paket-ID) = 6 Bit (Länge)$.
Mit 6 Bit lässt sich eine Anzahl von 0 bis $2^{6}=64$ Flits definieren.

All die obengenannten Daten werden nicht verschlüsselt,
sondern lediglich mit einem \ac{MAC} authentisiert.
Ein Headerflit enthält keine Nutzdaten.
Nach dem Empfang und der positiven Authentifikation der Routingdaten im Headerflit
wird ein \ac{ACK} an den Absender versendet.
Beim Nicht-Erhalten solch eines \ac{ACK}s nach dem Vergehen eines Time-outs
wiederholt der Absender den Sendevorgang.


\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{fig/header_flit.png}
	\caption{Struktur eines Headerflits}
	\label{fig:header_flit}
\end{figure}


Ein Body- bzw Tailflit (s. Abbildung \ref{fig:body_flit}) enthält die Flit-ID und die Nutzdaten,
die verschlüsselt und danach authentisiert werden.
Falls ein Paket nicht erhalten wurde bzw.
falls die Authenifikation fehlschlägt,
wird ein \ac{ARQ} mit Flit-ID und Paket-ID an den Absender versendet.

\begin{figure}[h]
	\centering
	\includegraphics[width=1\textwidth]{fig/body_flit.png}
	\caption{Struktur eines Body- bzw. Tailflits}
	\label{fig:body_flit}
\end{figure}


Die Authentisierung jedes einzelnen Flits ermöglicht die Retransmission einzelner Flits,
welche für eine effiziente Kommunikation in einem \ac{SoC} mit infizierten Mitgliedern ermöglicht.
Da die Hälfte jedes Flits aus dem \ac{MAC} besteht,
wird der Overhead mindestens die Hälfte der gesamten Daten sein.
Bei einer maximalen Paketanzahl von 64
ergeben sich folgende Anteile an Nutzdaten bzw. Overhead.
Die günstigste Proportion von Nutzdaten zu Gesamtdaten
mit $\frac{3584 Bit}{8320 Bit} = 0,43$
ergibt sich bei einer Nachricht,
die ohne Padding in 65 Flits aufgeteilt wird:
1 Headerflit mit 0 Bit Nutzdaten,
63 Bodyflits und 1 Tailflit mit jeweils 56 Bit Nutzdaten
ergeben in Summe $3584 Bit$ von Nutzdaten,
während die Gesamtgröße der Flits $65 * 128 Bit = 8320 Bit$ beträgt.

Die ungünstigte Proportion mit 0,031 ($\frac{8}{256} Bit$)
ergibt sich bei einer Nachrichtgröße 8 Bit:
ein Headerflit mit 0 Bit Nutzdaten und ein Tailflit mit 8 Bit Nutzdaten
ergeben 8 Bit Nutzdaten bei 256 Bit Gesamtgröße.
Auch wenn der Tailflit im letzten Fall kein Padding enthielte,
betrüge der Nutzdaten-Anteil 60 Bit und damit nur 23,4 \%.

Tabelle \ref{tab:haase_overhead} zeigt die Verteilung des Overheads bei Paketen verschiedener Größe
im beschriebenen Kommunikationsprotokoll:
bei einer sehr kleinen Anzahl der Flits (4 und weniger)
beträgt der Anteil der Nutzdaten nur weniger als ein Drittel der Gesamtdaten.

\begin{table}[h]
	\centering
	\caption{
		Größe des Overheads im Protokoll von \textit{Haase u.a.}
		mit Paketen verschiedener Größe (Zweierpotenzen).
		Kleine Pakete zeigen einen besonders hohen Anteil an Overhead.
		}
	\label{tab:haase_overhead}
	\renewcommand{\arraystretch}{1.3}
	\begin{tabular}{ccccc}
		\toprule
		Anzahl Flits      & Padding     & Nutzdaten, Bit & Gesamtdaten, Bit & Anteil Nutzdaten, \% \\ \midrule
		64                & nein        & 3528           & 8192             & 43,0 \\
		32                & nein        & 1736           & 4096             & 42,4 \\
		16                & nein        & 840            & 2048             & 41,0 \\
		8                 & nein        & 392            & 1024             & 38,0 \\
		4                 & nein        & 168            & 512              & 32,8 \\
		3                 & nein        & 112            & 384              & 29,2 \\
		2                 & nein        & 56             & 256              & 21,9 \\
		2                 & ja          & 1              & 256              & 3,1  \\ \bottomrule
	\end{tabular}
\end{table}


\subsection{Verbesserungsvorschläge}
In dem oben beschriebenen Protokoll
fehlt mit jedem Flit mitzusendende Unterscheidung,
ob es sich um einen Headerflit, Bodyflit oder Tailflit handelt.
Es sind in der internen Ausarbeitung (keine Quelle???)
2 Bits dafür vorgesehen,
die nicht verschlüsselt,
sondern lediglich authentisiert werden müssen.
Damit bleiben uns 60 Bit für die Nutzdaten übrig,
die verschlüsselt werden müssen.
Für die Verschlüssellung wird momentan die Blockchiffre PRINCE verwendet,
welche mindestens 64 Bit große Blöcke verschlüsseln kann.
Da PRINCE keine Nachrichten von benutzerdefinierter Größe verschlüsseln kann,
wird in dieser Arbeit nach anderen Verschlüsselungsmöglichkeiten recherchiert.
Diese Sorge betrifft nicht die Authenifikation,
da der gesamte Payload (inkl. Header-Informationen) signiert werden muss.

Die ermittelte Overhead-Größe der Pakete (s. Abbilung \ref{tab:haase_overhead})
ist insbesondere bei kurzen Nachrichten nicht optimal.
Da den größten Teil des Overheads die Authentisierung verursacht,
dürften andere Authentisierungs-Methoden in Betracht gezogen werden.

Einerseits kann PRINCE in einem Zyklus ein Datenblock ver- bzw. entschlüsseln
und ist damit optimal für kleine Delays,
anderseits wird für seine Implementierung viel Fläche auf dem Chip gebraucht \cite{volkens}.
Aus dieser Sicht wird die Suche eines anderen leichtgewichtigen kryptografischen Verfahrens angestrebt.

Damit haben sich zwei Aspekte herauskristallisiert,
die einer genaueren Untersuchung unterzogen werden dürfen:
die Verschlüsselung und die Authentifikation.
Wobei der Schwerpunkt dieser Arbeit in der Verschlüsselung bzw.
in der authentisierten Verschlüsselung liegen wird.
Die Herausvorderung bei der Suche nach Alternativen wird darin liegen,
dass das gesuchte Verfahren leichtgewichtig sein soll,
um eine effiziente Kommunikation im \ac{NoC} zu ermöglichen.

\subsection{Anforderungen ToDo}
Im nächsten Abschnitt werden exestierende Verfahren
--- die auf den ersten Blick als Lösung unseres Problems in Frage kommen können ---
beschrieben und begutachtet.
Dafür wollen wir die Anforderungen für unser zukünftiges Protokoll zusammenfassen.

Als erstes interessiert uns,
ob ein gegebenes Verfahren
nur für die Verschlüsselung,
oder gleichzeitig auch für die Authentifizierung
angewendet werden kann.

Dann wollen wir untersuchen,
ob das Verfahren
ausschließlich Klartexte einer begrenzten Größe bearbeiten kann,
oder ob da eine gewisse Flexibilität vorliegt.

Außerdem ist für uns wichtig,
dass das gesuchte Verfahren.

Die Effizienz bzw. Leichtgewichtigkeit eines Algorithmus
wollen wir mit solchen Kriterien, wie
Anzahl an Zyklen,
Flächenverbrauch und
Parallelisierbarket
unteruchen.

Das nach oberen Kriterien ausgewählte Verfahren
soll später gründlich unsersucht und ggf.
für unsere Anforderungen angepasst werden.
Eine Simulation des Verfahrens mit OMNeT++
wird uns erlauben,
das Resultat mit der gegebenen Lösung zu vergleichen.

!!! Das hier muss noch verbessert werden !!!
